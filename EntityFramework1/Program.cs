﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF1
{
    class Program
    {
        static void Main(string[] args)
        {
            
            using (StudyEntities se = new StudyEntities())
            {
                List<Employee> emp = se.Employees.ToList();
                emp.ForEach(_ => Console.WriteLine(JsonConvert.SerializeObject(_)));

                List<TICKET> tickets = se.TICKETS.ToList();
                tickets.ForEach(_ => Console.WriteLine(JsonConvert.SerializeObject(_)));

                List<ORDER> orders = se.ORDERS.ToList();
                orders.ForEach(_ => Console.WriteLine(JsonConvert.SerializeObject(_)));

                List<CUSTOMER> cust = se.CUSTOMERS.ToList();
                cust.ForEach(_ => Console.WriteLine(JsonConvert.SerializeObject(_)));

                List<Employee> emp_query_result = se.Database.SqlQuery<Employee>
                    ("SELECT * FROM EMPLOYEE WHERE ID=1").ToList();

                List<TICKET> movie_sp_result = se.Database.SqlQuery<TICKET>
                    ("exec SELECT_MOVIE_BY_HALL @hall=2").ToList();
            }


        }
    }
}
