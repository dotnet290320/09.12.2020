﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF1
{
    class Program
    {
        static void Main(string[] args)
        {

            int[] nums = { 1, 2, 3 };
            int item = nums.ToList().OrderByDescending(_ => _).First(_ => _ > 1);
            //<int> res_of_query = nums.ToList().OrderByDescending(_ => _).ToList();

            nums.ToList().OrderByDescending(_ => _).ToList().ForEach(_ => Console.WriteLine(_));

            using (StudyEntities1 se = new StudyEntities1())
            {
                // create a function which gets employee and add it add record
                Employee e = new Employee
                {
                    NAME = "Marry",
                    SALARY = 21_000
                };

                se.Employees.Add(e);

                se.SaveChanges(); // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!11

                // create a function which gets id and delete a record with this id
                List<Employee> emp = se.Employees.ToList();
                emp.ForEach(_ => Console.WriteLine(JsonConvert.SerializeObject(_)));

                Employee e1_id1 = emp.First(_ => _.ID == 1);
                Employee e2_id2 = emp.First(_ => _.ID == 1);

                se.Employees.OrderBy(_ => _.); // LINQ


                if (e1_id1 == e2_id2)
                {

                }
                //Employee emp_del = emp.First(_ => _.ID == 3);
                //emp.Remove(emp_del);

                se.Employees.Remove(se.Employees.First(_ => _.ID == 3));
                se.SaveChanges(); // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!11

                // *etgar: create a function which gets id + recrod and updated the db

                List<TICKET> tickets = se.TICKETS.ToList();
                tickets.ForEach(_ => Console.WriteLine(JsonConvert.SerializeObject(_)));

                List<ORDER> orders = se.ORDERS.ToList();
                orders.ForEach(_ => Console.WriteLine(JsonConvert.SerializeObject(_)));

                List<CUSTOMER> cust = se.CUSTOMERS.ToList();
                cust.ForEach(_ => Console.WriteLine(JsonConvert.SerializeObject(_)));

                List<Employee> emp_query_result = se.Database.SqlQuery<Employee>
                    ("SELECT * FROM EMPLOYEE WHERE ID=1").ToList();

                List<TICKET> movie_sp_result = se.Database.SqlQuery<TICKET>
                    ("exec SELECT_MOVIE_BY_HALL @hall=2").ToList();

               
            }

            Console.ReadLine();
        }
    }
}
