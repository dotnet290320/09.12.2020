//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EF1
{
    using System;
    
    public partial class FindCustomer_Result
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public int AGE { get; set; }
        public string ADDRESS { get; set; }
        public decimal SALARY { get; set; }
    }
}
